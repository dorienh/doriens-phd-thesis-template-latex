\select@language {english}
\vspace {-\cftbeforepartskip }
\contentsline {chapter}{\spacedlowsmallcaps {Acknowledgements}}{i}{chapter*.1}
\contentsline {chapter}{\spacedlowsmallcaps {List of Figures}}{vi}{chapter*.3}
\contentsline {chapter}{\spacedlowsmallcaps {List of Tables}}{viii}{chapter*.4}
\contentsline {chapter}{\spacedlowsmallcaps {Introduction}}{1}{chapter*.6}
\contentsline {part}{\numberline {1}\spacedlowsmallcaps {Your first part}}{3}{part.1}
\contentsline {chapter}{\numberline {1}\spacedlowsmallcaps {Top title of the chapter}}{5}{chapter.1}
\contentsline {part}{\numberline {2}\spacedlowsmallcaps {The other part: experiments}}{9}{part.2}
\contentsline {chapter}{\spacedlowsmallcaps {Conclusions}}{11}{chapter*.7}
\contentsline {chapter}{\spacedlowsmallcaps {Dutch summary}}{13}{chapter*.8}
\contentsline {chapter}{\numberline {A}\spacedlowsmallcaps {My title for appendix}}{15}{appendix.A}
\contentsline {chapter}{\numberline {B}\spacedlowsmallcaps {Another appendix}}{17}{appendix.B}
\contentsline {chapter}{\numberline {C}\spacedlowsmallcaps {List of publications}}{19}{appendix.C}
\vspace {\beforebibskip }
\contentsline {chapter}{\spacedlowsmallcaps {Bibliography}}{21}{appendix.C}
\contentsline {chapter}{\spacedlowsmallcaps {Index}}{22}{chapter*.9}
\contentsfinish 
